#!/usr/bin/env python
# encoding: utf-8
import json
from flask import Flask, request
from flask_cors import CORS, cross_origin
import requests

app = Flask(__name__)
CORS(app, support_credentials=True)


def filter(rsid):
    try:
        url = f"https://www.ebi.ac.uk/gwas/rest/api/singleNucleotidePolymorphisms/{rsid}/studies"
        res = requests.get(url).json()
        for study in res["_embedded"]["studies"]:
            population = []
            for s in study["ancestries"]:
                if s["numberOfIndividuals"] >= 1000:
                    population.append(s["numberOfIndividuals"])
            if len(population) >= 2:
                return True
            else:
                return False
    except:
        pass
    return False


@app.route('/search')
@cross_origin(supports_credentials=True)
def search():
    search_term = request.args.get("term").replace(" ", "+")
    x = requests.get(
        f'https://api.epigraphdb.org/meta/nodes/Disease/search?name={search_term}&limit={request.args.get("limit")}&full_data=true').json()
    return x


@app.route('/getVariants')
@cross_origin(supports_credentials=True)
def getVariants():
    id = request.args.get("id")
    query = {
        "query": "MATCH (disease:Disease)-[r:GENE_TO_DISEASE]-(gene:Gene)-[r2:VARIANT_TO_GENE]-("
                 "variant:Variant) WHERE "
                 f"disease.id=\"{id}\" RETURN variant;"
    }
    res = requests.post("https://api.epigraphdb.org/cypher", json=query).json()["results"]
    result = {
        "rsids": [],
        "filter": []
    }
    for i, x in enumerate(res):
        rsid = x["variant"]["name"]
        if filter(rsid):
            result["filter"].append({i: "1"})
        else:
            result["filter"].append({i: "0"})
        result["rsids"].append({i: x})
    return result


if __name__ == '__main__':
    app.run()
